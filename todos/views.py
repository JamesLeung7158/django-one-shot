from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def show_todo_list(request):
    todolist = TodoList.objects.all()
    context = {"list": todolist}
    return render(request, "lists/list.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {"detail": detail}
    return render(request, "lists/detail.html", context)
    # return redirect(context, "todo_list_detail", id=list.id)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            detail = form.save()
            return redirect("todo_list_detail", id=detail.id)
    else:
        form = TodoListForm()
    context = {"form": form}

    return render(request, "forms/create.html", context)


def todo_list_update(request, id):
    detail = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=detail)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=detail)
    context = {"form": form}
    return render(request, "forms/update.html", context)


def todo_list_delete(request, id):
    detail = TodoList.objects.get(id=id)
    if request.method == "POST":
        detail.delete()
        return redirect("todo_list_list")
    return render(request, "forms/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "forms/createitem.html", context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.list.id)
    else:
        form = TodoItemForm(instance=item)
    context = {"form": form}
    return render(request, "forms/updateitem.html", context)
